#!/bin/bash
export MYSQL_DB=DBNAME
export MYSQL_USER=USERNAME
export MYSQL_PASS=PASSWORD
export MYSQL_HOST='MYSQL_SERVER'
export MQTT_SERVER=MQTT_SERVER
export MQTT_PORT=MQTT_PORT

/usr/bin/python3 /app/gwunw_cg-97_coulometer_reader_py/mqtt_ssl_meansures_reader.py "/pzem017/+/meansures", "/CG97/+/meansures"