#!/usr/bin/env python3
from ctypes import Array
import serial
import time
import codecs 

#CWUNW Coulometer model CG-97 protocol
# 1. Equipment data output mode.
# Baud Rate: 19200 Check Bit: NONE Data Bit: 8 Stop Bit: 1
# Each output will output a set of 44 bytes of device information.
# Practical examples: 　 FE FE FE FE 00 03 5A 98 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 03 F0
# Number correspondence: 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
# 01 02: Fixed as FEFEFE Data Group Starting Mark
# 03 04: Fixed as FEFEFE Data Group Starting Mark
# 05: The default factory address is 0.
# 06: Percentage of electricity
# 07 08: Temperature value int
# 09 10: Voltage high bytes
# 11 12: Voltage Value Low Byte Variable Format Unsigned Long Note: The conversion result is in 10mV units
# 13 14: Current Value High Bytes
# 15 16: Current Value Low Byte Variable Format Unsigned Long Note: The conversion result is in 10mA units
# 17   : The positive and negative value of current 0 is positive 1 is negative
# 18 19: High bytes of electrical value
# 20 21: Low byte variable format for power value is Unsigned long Note: The conversion result is in 1mAH units
# 22 23: High bytes of time value
# 24 25: Time Value Low Byte Variable Format Unsigned Long Note: The conversion result is in 1 minute units
# 26 27: High power data bytes
# 28 29: Low byte variable format for power data is Unsigned long Note: Conversion results and 30H values are determined jointly
# 30   :Power value decimal point position power data from the right decimal point position (power data such as less than 6 bits before adding 0 to fill 6 bits)
# 31   : All bytes before the check value are added
 
# Explain:
# Each time the instrument outputs a set of information, which can be filtered and selected for its own use.
# 2. How to issue instructions to equipment
# All instructions are marked with 7733 as the beginning
# 1. To issue zero-clearing power instructions to equipment
# Practical example:     77 33 C0 03
# Number correspondence: 01 02 03 04
# 01 02: Identification Header 03: Device Address + 0xC4: This Record Zero Clearing Instruction Code 0x03
# 2. Send instructions to the device to change the data output mode
# Practical example:     77 33 C0 41
# Number correspondence: 01 02 03 04
# 01 02: Identification Header 03: Device Address + 0xC4: Instruction Code [40 - Stop Output 41 - Output a set of data 42 - Start Continuous Output Data] Outgoing by default 40
# 3. Issue instructions to fix the address of the equipment:
# Practical example:     77 33 C0 81
# Number correspondence: 01 02 03 04
# 01 02: Identification Header 03: Device Address + 0xC4: Instruction Code [New Address = 0x81-0x80] Address Range 0x00-0x7f Total 128 So Instruction Code Range is 0x80-0xff
# 4. To issue mandatory instructions to the equipment:
# Practical example:     77 33 8A
# Number correspondence: 01 02 03
# 01 02: Identification Header 03: Instruction Code [8A-Unconditional Stop Output 8B-Unconditional Single Output Set of Data
# This command ignores the device address and can be used to view the device address when it is forgotten that the device address cannot be controlled.

#Samples
# FE FE FE FE 01 54 01 0E 00 00 00 00 00 00 00 07 01 00 00 3B A7 00 00 00 00 00 00 00 00 04 4A
# FE FE FE FE 01 54 01 08 00 00 04 D3 00 00 00 00 00 00 00 3B A4 00 00 00 01 00 00 00 00 03 10
# FE FE FE FE 01 54 01 08 00 00 04 D3 00 00 00 01 00 00 00 3B A4 00 00 00 01 00 00 00 7B 03 8C
# FE FE FE FE 01 54 01 09 00 00 04 D3 00 00 00 02 00 00 00 3B A4 00 00 00 01 00 00 00 F6 03 09 
# FE FE FE FE 01 54 01 09 00 00 04 D3 00 00 00 02 00 00 00 3B A3 00 00 00 01 00 00 00 F6 03 08 
# FE FE FE FE 01 54 01 09 00 00 04 D2 00 00 00 01 00 00 00 3B A3 00 00 00 01 00 00 00 7B 03 8B 
# FE FE FE FE 01 54 01 09 00 00 04 D2 00 00 00 02 00 00 00 3B A3 00 00 00 01 00 00 00 F6 03 07 
# fe fe fe fe 01 64 01 06 00 00 04 d1 00 00 00 02 00 00 00 46 50 00 00 12 05 00 00 00 f6 03 e1

# byte 06: Percentage of electricity
def read_percentage(response: Array):
    if (response[5]):
        batery_percentage = response[5]
        print('Batery = {batery_percentage} %'.format(batery_percentage=batery_percentage))

# bytes 07 08: Temperature value int
def read_temperarure(response: Array):
    hexa_value = response[6:8]
    if (hexa_value):
        temperature = int(codecs.encode(hexa_value, 'hex_codec'), 16) / 10.0
        print('Temperature = {temperature} Ceusius'.format(temperature=temperature))

# bytes 09 10: Voltage high bytes
# bytes 11 12: Voltage Value Low Byte Variable Format Unsigned Long Note: The conversion result is in 10mV units
def read_voltage(response: Array):
    hexa_value = response[8:12]
    if (hexa_value):
        voltage_high_bytes = int(codecs.encode(hexa_value[0:2], 'hex_codec'), 16) 
        voltage_low_bytes = int(codecs.encode(hexa_value[2:], 'hex_codec'), 16) * 0.01
        voltage = voltage_high_bytes + voltage_low_bytes
        print('Voltage = {voltage} V'.format(voltage=voltage))

# 13 14: Current Value High Bytes
# 15 16: Current Value Low Byte Variable Format Unsigned Long Note: The conversion result is in 0..units
# 17   : The positive and negative value of current 0 is positive 1 is negative
def read_current(response: Array):
    hexa_value = response[12:18]
    if (hexa_value):
        current_high_bytes = int(codecs.encode(hexa_value[0:2], 'hex_codec'), 16) 
        current_low_bytes = int(codecs.encode(hexa_value[2:4], 'hex_codec'), 16) * 0.01
        current_signal = hexa_value[4]
        current = current_high_bytes + current_low_bytes
        if (current_signal == 1):
            current = current * -1
        print('Current = {current} A'.format(current=current))

# 18 19: High bytes of electrical value
# 20 21: Low byte variable format for power value is Unsigned long Note: The conversion result is in 1mAH units
def read_electrical_value(response: Array):
    hexa_value = response[17:21]
    if (hexa_value):
        electrical_value_high_bytes = int(codecs.encode(hexa_value[0:2], 'hex_codec'), 16) 
        electrical_value_low_bytes = int(codecs.encode(hexa_value[2:4], 'hex_codec'), 16) * 0.001
        electrical_value = electrical_value_high_bytes + electrical_value_low_bytes
        print('Eletrical Values = {electrical_value} AH'.format(electrical_value=electrical_value))

# 22 23: High bytes of time value
# 24 25: Time Value Low Byte Variable Format Unsigned Long Note: The conversion result is in 1 minute units
def read_time(response: Array):
    hexa_value = response[21:25]
    if (hexa_value):
        time_high_bytes = int(codecs.encode(hexa_value[0:2], 'hex_codec'), 16) 
        time_low_bytes = int(codecs.encode(hexa_value[2:4], 'hex_codec'), 16) 
        time = time_high_bytes + time_low_bytes
        print('Time = {time} min'.format(time=time))


# 26 27: High power data bytes
# 28 29: Low byte variable format for power data is Unsigned long Note: Conversion results and 30H values are determined jointly
# 30   :Power value decimal point position power data from the right decimal point position (power data such as less than 6 bits before adding 0 to fill 6 bits)
def read_power(response: Array):
    hexa_value = response[25:30]
    if (hexa_value):
        power_high_bytes = int(codecs.encode(hexa_value[0:2], 'hex_codec'), 16) 
        power_low_bytes = int(codecs.encode(hexa_value[2:4], 'hex_codec'), 16)
        power_decimal_point_position = hexa_value[4]
        power = (power_high_bytes + power_low_bytes) / int("1".ljust(power_decimal_point_position + 1, '0'))
        print('Power = {power} W'.format(power=power))

# 31   : All bytes before the check value are added
def is_valid_response(response: Array) -> bool:
    valid_response = False
    if (response and len(response) == 31):
        checked_value = 0
        for value in response[:-1]:
            sum_value = (value + checked_value)
            if (
                    (256 <= (sum_value - 256)) or
                    ((sum_value - 256) < 0)
                ):
                checked_value = sum_value
            else:
                checked_value = (sum_value - 256)
            

        valid_response = (checked_value == response[-1])

    return valid_response



ser=serial.Serial('COM3', 19200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=0)

# 4. To issue mandatory instructions to the equipment:
# Practical example:     77 33 8A
# Number correspondence: 01 02 03
# 01 02: Identification Header 03: Instruction Code [8A-Unconditional Stop Output 8B-Unconditional Single Output Set of Data
ser.write(bytearray(b"\x77\x33\x8A"))




while True:
    # 2. Send instructions to the device to change the data output mode
    # Practical example:     77 33 C0 41
    # Number correspondence: 01 02 03 04
    # 01 02: Identification Header 03: Device Address + 0xC4: Instruction Code [40 - Stop Output 41 - Output a set of data 42 - Start Continuous Output Data] Outgoing by default 40
    ser.write(bytearray(b"\x77\x33\xC1\x40")) #ask for device address C1 to Stop Output 
    ser.write(bytearray(b"\x77\x33\xC1\x41")) #ask for device address C1 for data
    response = ser.read_all()

    if (is_valid_response(response)):
        read_temperarure(response)
        read_percentage(response)
        read_voltage(response)
        read_current(response)
        read_electrical_value(response)
        read_time(response)
        read_power(response)
    time.sleep(5)


