from __future__ import print_function
from __future__ import unicode_literals
from builtins import str
import os
import sys
import paho.mqtt.client as mqtt
from threading import Thread, Event
from meansures_reader import MeansureReader

event_client_connected = Event()
event_stop_client = Event()
meansures_classes = {}
topic_names = []


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client: mqtt.Client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    event_client_connected.set()
    for topic_name in topic_names:
        client.subscribe(topic=topic_name)


def mqtt_client_task(client: mqtt.Client):
    while not event_stop_client.is_set():
        client.loop()
   

def on_subscribe(client: mqtt.Client, userdata, mid, reasonCodes, properties=None):
    print("Subscribed: " + str(mid))

# The callback for when a PUBLISH message is received from the server.
def on_message(client: mqtt.Client, userdata, msg):
    global meansures_classes
    if not msg.topic in meansures_classes:
       meansures_classes[msg.topic] = MeansureReader(topic_name=msg.topic,  
                                                    mysql_user=os.environ['MYSQL_USER'],
                                                    mysql_password=os.environ['MYSQL_PASS'],
                                                    mysql_host=os.environ['MYSQL_HOST'],
                                                    mysql_database=os.environ['MYSQL_DB'])
    
    meansures_classes[msg.topic].save_meansures(meansures_payload = msg.payload.decode())


def start_mqtt_meansures_read():
    
    # Look for host:port in sdkconfig
    broker_url = os.environ['MQTT_SERVER']
    broker_port = int(os.environ['MQTT_PORT'])
    client = None
    # 1. Test connects to a broker
    try:
        client = mqtt.Client()
        client.on_connect = on_connect
        client.on_message = on_message
        client.on_subscribe = on_subscribe
        # client.user_data_set(binary_file)

        ca_certs_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "certs",
            "mqtt-ca.crt")

        certfile_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "certs",
            "mqtt-consumer.crt")

        keyfile_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "certs",
            "mqtt-consumer.key")

        client.tls_set(
            ca_certs=ca_certs_path,
            certfile=certfile_path,
            keyfile=keyfile_path)

        # client.tls_set(None,
        #                None,
        #                None, cert_reqs=ssl.CERT_NONE, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
        # client.tls_insecure_set(True)
        print("Connecting...")
        client.connect(broker_url, broker_port, 60)
    except Exception:
        print("ENV_TEST_FAILURE: Unexpected error while connecting to broker {}: {}:".format(broker_url, sys.exc_info()[0]))
        raise
    # Starting a py-client in a separate thread
    thread1 = Thread(target=mqtt_client_task, args=(client,))
    thread1.start()
    thread1.join()


if __name__ == '__main__':
    for topic_name in sys.argv[1:]:
        topic_names.append(topic_name)
    start_mqtt_meansures_read()
