#!/usr/bin/env python3
import mariadb
import os
from datetime import datetime

class MeansureReader(object):
    CG97_COULOMETER = 1
    PZEM017_V1 = 2
    
    def __init__(self, 
                topic_name: str,
                mysql_user: str,
                mysql_password: str,
                mysql_host: str,
                mysql_database: str) -> None:
        self.topic_name = topic_name
        self.__user=mysql_user  
        self.__password=mysql_password
        self.__host=mysql_host
        self.__database=mysql_database

    def get_connection(self):
        return mariadb.connect(
            user=self.__user,
            password=self.__password,
            host=self.__host,
            database=self.__database)

    def get_cod_equi_meda(self) -> int:
        if not hasattr(self, 'cod_equi_meda'):
            self.cod_equi_meda = self.topic_name.split("/")[2]
        return self.cod_equi_meda


    def get_cod_tipo_inst_meda(self) -> int:
        if not hasattr(self, 'cod_tipo_inst_meda'):
            cur = self.__execute_mysql_command(sql_command="SELECT COD_TIPO_INST_MEDA FROM TB007_EQUI_MEDA WHERE COD_EQUI_MEDA = %s", 
                                                params=(self.get_cod_equi_meda(),),
                                                auto_close=False)
            result = cur.fetchone()
            if result:
                self.cod_tipo_inst_meda = result[0]
                cur.connection.close()
       
        return self.cod_tipo_inst_meda
        
    def get_cod_tipo_medicao_local(self) -> int:
        if not hasattr(self, 'cod_tipo_medicao_local'):
            cur = self.__execute_mysql_command(sql_command="SELECT COD_TIPO_MEDA_LOCA FROM TB004_TIPO_MEDA_LOCA WHERE COD_EQUI_MEDA = %s", 
                                                params=(self.get_cod_equi_meda(),),
                                                auto_close=False
                                                )
            result = cur.fetchone()
            if result:
                self.cod_tipo_medicao_local = result[0]
                cur.connection.close()
      
        return self.cod_tipo_medicao_local


    def save_cg97_menadures(self, meansure_id, meansures) -> None:
        self.__execute_mysql_command(sql_command="""INSERT INTO TB005_MEDA_BATE_CG97 
                        (
                            COD_MEDA,
                            VLR_TENS,
                            VLR_CORR,
                            VLR_POTE,
                            VLR_TEMP,
                            NUM_MINU,
                            NUM_PERC,
                            VLR_ELET
                        )
                    VALUES 
                    (?, ?, ?, ?, ?, ?, ?, ?)""", 
                       params=(
                            meansure_id,
                            meansures[3], #VLR_TENS ==> 12.300000
                            meansures[4], #VLR_CORR ==> 0.070000
                            meansures[5], #VLR_POTE ==> 0.861000
                            meansures[1], #VLR_TEMP ==> 27.900000
                            meansures[0], #NUM_MINU ==> 4490
                            meansures[2], #NUM_PERC ==> 100
                            meansures[6]  #VLR_ELET ==> 18.000000
                        ),
                        auto_commit=True
            )
            
       

    def save_pzem017_v1_menadures(self, meansure_id, meansures) -> None:
        self.__execute_mysql_command(sql_command="""INSERT INTO TB008_MEDA_VATE_PZEM017 
                            (
                                COD_MEDA,
                                VLR_TENS,
                                VLR_CORR,
                                VLR_POTE,
                                VLR_ENER,
                                IND_HIGH_VOLT_ALAR,
                                IND_LOW_VOLR_ALAR
                            )
                        VALUES 
                        (?, ?, ?, ?, ?, ?, ?)""", 
                            params=(
                                meansure_id,
                                meansures[0], #VLR_TENS ==> 12.300000
                                meansures[1], #VLR_CORR ==> 0.410000
                                meansures[2], #VLR_POTE ==> 5.200000
                                meansures[3], #VLR_ENER ==> 0.000000
                                meansures[4]=='1', #IND_HIGH_VOLT_ALAR ==> 0
                                meansures[5]=='1', #IND_LOW_VOLR_ALAR ==> 0
                            ),
                            auto_commit=True
                )
    
    

    def __execute_mysql_command(self, sql_command: str, params: tuple, auto_commit: bool=False, auto_close: bool=True):
        conn = self.get_connection()    
        cur = conn.cursor() 
        #insert information 
        try: 
            cur.execute(sql_command, params) 
        except mariadb.Error as e: 
            print(f"Error: {e}")
        if auto_commit:
            conn.commit()
        
        if auto_close:
            conn.close()

        return cur


    def save_meansures(self, meansures_payload):

        now = datetime.utcnow() #   time.strftime('%Y-%m-%d %H:%M:%S')

        cod_tipo_medicao_local = self.get_cod_tipo_medicao_local()
        cod_tipo_inst_meda = self.get_cod_tipo_inst_meda()

        if cod_tipo_medicao_local > 0:
            cur = self.__execute_mysql_command(sql_command="INSERT INTO TB003_MEDA (DAT_HOR_MEDA, COD_TIPO_MEDA_LOCA) VALUES (?, ?)", 
                                                params=(now, cod_tipo_medicao_local),
                                                auto_commit=True)
            meansure_id = cur.lastrowid
            print(f"Last Inserted ID: {meansure_id}")

            meansures = meansures_payload.split(";")
            if cod_tipo_inst_meda == self.CG97_COULOMETER:
                self.save_cg97_menadures(meansure_id=meansure_id, meansures=meansures)
            elif cod_tipo_inst_meda == self.PZEM017_V1:
               self.save_pzem017_v1_menadures(meansure_id=meansure_id, meansures=meansures)

            print(f"{now} - Last Inserted meansures: id: {meansure_id}, {meansures}")
        else:
            print("cod_tipo_medicao_local not found")

        